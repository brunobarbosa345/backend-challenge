ARG deploy_env='production'

FROM node:10-alpine

RUN apk add yarn

WORKDIR /

EXPOSE 3002

COPY package.json .

RUN yarn

COPY . .

ARG deploy_env

ENV DEPLOY_ENV ${deploy_env}

CMD ["yarn", "build"]