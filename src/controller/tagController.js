import response from "services/response"
import { tagBusiness } from "business"

class tagController {
  constructor() {}
  async find(id) {
    try {
      return await tagBusiness.find(id)
    } catch (err) {
      return response.error(err, { message: "Error controller find" })
    }
  }
  async findAll() {
    try {
      return await tagBusiness.findAll()
    } catch (err) {
      return response.error(err, { message: "Error controller findAll" })
    }
  }
  async create(body) {
    try {
      return await tagBusiness.create(body)
    } catch (err) {
      return response.error(err, { message: "Error controller create" })
    }
  }
  async update(id, body) {
    try {
      return await tagBusiness.update(id, body)
    } catch (err) {
      return response.error(err, { message: "Error controller update" })
    }
  }

  async delete(id) {
    try {
      return await tagBusiness.delete(id)
    } catch (err) {
      return response.error(err, { message: "Error controller delete" })
    }
  }
}

export default new tagController()
