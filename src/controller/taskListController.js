import response from "services/response"
import { taskListBusiness } from "business"

class taskListController {
  constructor() {}
  async find(id) {
    try {
      return await taskListBusiness.find(id)
    } catch (err) {
      return response.error(err, { message: "Error controller find" })
    }
  }
  async findAll() {
    try {
      return await taskListBusiness.findAll()
    } catch (err) {
      return response.error(err, { message: "Error controller findAll" })
    }
  }
  async create(body) {
    try {
      return await taskListBusiness.create(body)
    } catch (err) {
      return response.error(err, { message: "Error controller create" })
    }
  }
  async update(id, body) {
    try {
      return await taskListBusiness.update(id, body)
    } catch (err) {
      return response.error(err, { message: "Error controller update" })
    }
  }

  async delete(id) {
    try {
      return await taskListBusiness.delete(id)
    } catch (err) {
      return response.error(err, { message: "Error controller delete" })
    }
  }
}

export default new taskListController()
