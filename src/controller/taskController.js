import response from "services/response"
import { taskBusiness } from "business"

class taskController {
  constructor() {}
  async find(id) {
    try {
      return await taskBusiness.find(id)
    } catch (err) {
      return response.error(err, { message: "Error controller find" })
    }
  }
  async findAll() {
    try {
      return await taskBusiness.findAll()
    } catch (err) {
      return response.error(err, { message: "Error controller findAll" })
    }
  }
  async create(body) {
    try {
      return await taskBusiness.create(body)
    } catch (err) {
      return response.error(err, { message: "Error controller create" })
    }
  }
  async update(id, body) {
    try {
      return await taskBusiness.update(id, body)
    } catch (err) {
      return response.error(err, { message: "Error controller update" })
    }
  }

  async delete(id) {
    try {
      return await taskBusiness.delete(id)
    } catch (err) {
      return response.error(err, { message: "Error controller delete" })
    }
  }
}

export default new taskController()
