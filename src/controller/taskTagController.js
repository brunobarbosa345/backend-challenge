import response from "services/response"
import { taskTagBusiness } from "business"

class taskTagController {
  constructor() {}
  async find(id) {
    try {
      return await taskTagBusiness.find(id)
    } catch (err) {
      return response.error(err, { message: "Error controller find" })
    }
  }
  async findAll() {
    try {
      return await taskTagBusiness.findAll()
    } catch (err) {
      return response.error(err, { message: "Error controller findAll" })
    }
  }
  async create(body) {
    try {
      return await taskTagBusiness.create(body)
    } catch (err) {
      return response.error(err, { message: "Error controller create" })
    }
  }
  async update(id, body) {
    try {
      return await taskTagBusiness.update(id, body)
    } catch (err) {
      return response.error(err, { message: "Error controller update" })
    }
  }

  async delete(id) {
    try {
      return await taskTagBusiness.delete(id)
    } catch (err) {
      return response.error(err, { message: "Error controller delete" })
    }
  }
}

export default new taskTagController()
