import response from "services/response"
import db from "database/models"

const { taskModel, taskTagModel } = db

class taskBusiness {
  constructor() {}
  async find(id) {
    try {
      return taskModel
        .findOne({ where: { id }, include: [{ model: taskTagModel }] })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error find" })
    }
  }
  async findAll() {
    try {
      return taskModel
        .findAll({ include: [{ model: taskTagModel }] })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error findAll" })
    }
  }
  async create(body) {
    try {
      return taskModel
        .create(body, { returning: true })
        .then((data) => response.success(data, { status: 201 }))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error create" })
    }
  }
  async update(id, body) {
    try {
      return taskModel
        .update(body, { where: { id } }, { returning: true })
        .then(async (result) =>
          response.success(await taskModel.findOne({ where: { id } }, { raw: true }))
        )
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error update" })
    }
  }

  async delete(id) {
    try {
      return taskModel
        .destroy({ where: { id } })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error delete" })
    }
  }
}

export default new taskBusiness()
