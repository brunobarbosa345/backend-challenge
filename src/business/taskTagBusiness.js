import response from "services/response"
import db from "database/models"

const { taskTagModel } = db

class taskTagBusiness {
  constructor() {}
  async find(id) {
    try {
      return taskTagModel
        .findOne({ where: { id } })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error find" })
    }
  }
  async findAll() {
    try {
      return taskTagModel
        .findAll()
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error findAll" })
    }
  }
  async create(body) {
    try {
      return taskTagModel
        .create(body, { returning: true })
        .then((data) => response.success(data, { status: 201 }))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error create" })
    }
  }
  async update(id, body) {
    try {
      return taskTagModel
        .update(body, { where: { id } }, { returning: true })
        .then(async (result) =>
          response.success(await taskTagModel.findOne({ where: { id } }, { raw: true }))
        )
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error update" })
    }
  }

  async delete(id) {
    try {
      return taskTagModel
        .destroy({ where: { id } })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error delete" })
    }
  }
}

export default new taskTagBusiness()
