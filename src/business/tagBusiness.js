import response from "services/response"
import db from "database/models"

const { tagModel, taskTagModel } = db

class tagBusiness {
  constructor() {}
  async find(id) {
    try {
      return tagModel
        .findOne({ where: { id }, include: [{ model: taskTagModel }] })
        .then((data) => response.success({ ...data.dataValues, count: data.TaskTags.length }))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error find" })
    }
  }
  async findAll() {
    try {
      return tagModel
        .findAll({ include: [{ model: taskTagModel }] })
        .then((data) =>
          response.success(data.map((d) => ({ ...d.dataValues, count: d.TaskTags.length })))
        )
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error findAll" })
    }
  }
  async create(body) {
    try {
      return tagModel
        .create(body, { returning: true })
        .then((data) => response.success(data, { status: 201 }))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error create" })
    }
  }
  async update(id, body) {
    try {
      return tagModel
        .update(body, { where: { id } }, { returning: true })
        .then(async (result) =>
          response.success(await tagModel.findOne({ where: { id } }, { raw: true }))
        )
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error update" })
    }
  }

  async delete(id) {
    try {
      return tagModel
        .destroy({ where: { id } })
        .then((data) => response.success(data))
        .catch((err) => response.error(err))
    } catch (err) {
      return response.error(err, { message: "Error delete" })
    }
  }
}

export default new tagBusiness()
