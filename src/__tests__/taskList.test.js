import axios from "axios"
import faker from "faker"

require("services/environment")

faker.locale = "pt_BR"
jest.setTimeout(30000)

const { SERVER_URL } = process.env
const pathRoute = `${SERVER_URL}/v1/task-list`

let bodyObject = { name: "taskList-name-zuzu" }
let objectCreated = null

beforeAll(() => {})
afterAll(() => {})
beforeEach(() => {})
afterEach(() => {})

test("create", async (done) => {
  objectCreated = await axios
    .post(`${pathRoute}`, bodyObject)
    .then((result) => {
      expect(result.status).toBe(201)
      expect(result.data.hasError).toBeFalsy()
      expect(result.data.data).toBeDefined()
      expect(result.data.data.name).toBe(bodyObject.name)
      //save object to use later
      return result.data.data
    })
    .catch((err) => {
      console.log(err)
      throw "Error create object"
    })
  done()
})

test("find", (done) => {
  axios
    .get(`${pathRoute}/${objectCreated.id}`)
    .then((result) => {
      expect(result.status).toBe(200)
      expect(result.data.hasError).toBeFalsy()
      expect(result.data.data.id).toBeDefined()
    })
    .catch((err) => {
      console.log(err)
      throw "Error find object"
    })

  done()
})

test("update", (done) => {
  const nameFake = faker.name.findName()
  axios
    .put(`${pathRoute}/${objectCreated.id}`, { name: nameFake })
    .then((result) => {
      expect(result.status).toBe(200)
      expect(result.data.hasError).toBeFalsy()
    })
    .catch((err) => {
      console.log(err)
      throw "Error update object"
    })
  done()
})

test("delete", async (done) => {
  await axios
    .delete(`${pathRoute}/${objectCreated.id}`)
    .then(async (result) => {
      expect(result.status).toBe(200)
      expect(result.data.hasError).toBeFalsy()
    })
    .catch((err) => {
      console.log(err)
      throw "Error update object"
    })
  done()
})
