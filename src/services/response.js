import axios from "axios"

class response {
  constructor() {
    this.debug = process.env.RESPONSE_DEBUG || false
    this.url = process.env.RESPONSE_URL || false
    this.data = {}
  }

  info() {
    return this.data
  }

  async sendFeedback() {
    if (this.url)
      try {
        axios
          .post(this.url, this.data)
          .then((result) => console.log("service > response > sendFeedback : SUCCESS"))
          .catch((err) => console.error("service > response > sendFeedback : ERROR", err))
      } catch (error) {
        console.error("service > response > sendFeedback : ERROR", error)
      }
  }

  success(data = {}, options = {}) {
    const { message = "", status = 200 } = options
    this.data = {
      hasError: false,
      status: status || 200,
      message: (data && data.message) || message || "",
      data,
    }

    this.sendFeedback()
    this.showDebug()

    return this.data
  }

  error(error = {}, options = {}) {
    const { message = "", status = 400 } = options
    this.data = {
      hasError: true,
      status: status || 400,
      message: (error && error.message) || message || "",
      error,
    }

    this.sendFeedback()
    this.showDebug()

    return this.data
  }

  showDebug() {
    if (this.debug)
      try {
        console.log("RESPONSE_DEBUG::\n", JSON.parse(JSON.stringify(this.data)))
      } catch (error) {
        console.error("ERROR SHOW DEBUG")
      }
  }
}

export default new response()
