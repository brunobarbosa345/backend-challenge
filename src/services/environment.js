import path from "path"
import * as dotenv from "dotenv"

export default dotenv.config({
  path: `${path.join(__dirname, "../..", `.env.${process.env.NODE_ENV}`)}`,
})
