import modelOptions from "../config/modelOptions"
export default (sequelize, DataTypes) =>
  sequelize.define(
    "TaskTag",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      taskId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Task",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },
      tagId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Tag",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },

      createdAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      //sequelize,
      modelName: "TaskTag",
      ...modelOptions,
      // options,
    }
  )
