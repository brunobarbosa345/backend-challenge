import modelOptions from "../config/modelOptions"
export default (sequelize, DataTypes) =>
  sequelize.define(
    "TaskList",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },

      createdAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      //sequelize,
      modelName: "TaskList",
      ...modelOptions,
      // options,
    }
  )
