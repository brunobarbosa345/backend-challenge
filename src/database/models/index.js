import fs from "fs"
import path from "path"
import Sequelize from "sequelize"
import { DataTypes } from "sequelize"

import config from "../config/config"

const db = {}

const { database, username, password } = config

const sequelize = new Sequelize(database, username, password, { ...config })

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== path.basename(__filename) && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

import taskModel from "./task"
import tagModel from "./tag"
import taskListModel from "./taskList"
import taskTagModel from "./taskTag"

db.taskModel = taskModel(sequelize, DataTypes)
db.tagModel = tagModel(sequelize, DataTypes)
db.taskListModel = taskListModel(sequelize, DataTypes)
db.taskTagModel = taskTagModel(sequelize, DataTypes)

/*Relations 1-1*/
db.taskListModel.hasOne(db.taskModel, {
  foreignKey: "taskListId",
  onUpdate: "CASCADE",
})
db.taskModel.belongsTo(db.taskListModel, {
  foreignKey: "taskListId",
  onUpdate: "CASCADE",
})

/* resolution taskTag */
db.taskModel.hasMany(db.taskTagModel, {
  foreignKey: "taskId",
  onUpdate: "CASCADE",
})
db.taskTagModel.belongsTo(db.taskModel, {
  foreignKey: "taskId",
  onUpdate: "CASCADE",
})
db.tagModel.hasMany(db.taskTagModel, {
  foreignKey: "tagId",
  onUpdate: "CASCADE",
})
db.taskTagModel.belongsTo(db.tagModel, {
  foreignKey: "tagId",
  onUpdate: "CASCADE",
})

/*Relations 1-0.N Resolution*/
db.taskModel.belongsToMany(db.taskListModel, {
  through: "taskListTaskModel",
  as: "taskLists",
  foreignKey: "taskId",
  onUpdate: "CASCADE",
})
db.taskListModel.belongsToMany(db.taskModel, {
  through: "taskListTaskModel",
  as: "tasks",
  foreignKey: "taskId",
  onUpdate: "CASCADE",
})

export default db
