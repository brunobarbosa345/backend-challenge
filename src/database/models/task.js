import modelOptions from "../config/modelOptions"
export default (sequelize, DataTypes) =>
  sequelize.define(
    "Task",
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      notes: {
        type: DataTypes.STRING,
      },
      priority: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      remindMeOn: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      activityType: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ["indoors", "outdoors"],
      },
      status: {
        type: DataTypes.ENUM,
        values: ["open", "done"],
        defaultValue: "open",
      },
      tags: {
        type: DataTypes.STRING,
        defaultValue: "",
      },

      taskListId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "TagList",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },

      createdAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: DataTypes.DATE,
      },
      deletedAt: {
        type: DataTypes.DATE,
        defaultValue: null,
      },
    },
    {
      //sequelize,
      modelName: "Task",
      ...modelOptions,
      // options,
    }
  )
