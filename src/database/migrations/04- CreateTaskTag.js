module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("TaskTag", {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      taskId: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Task",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },
      tagId: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Tag",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },

      createdAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      deletedAt: {
        type: Sequelize.DataTypes.DATE,
        defaultValue: null,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("TaskTag")
  },
}
