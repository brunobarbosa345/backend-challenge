module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Tag", {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
      },
      count: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },

      createdAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      deletedAt: {
        type: Sequelize.DataTypes.DATE,
        defaultValue: null,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Tag")
  },
}
