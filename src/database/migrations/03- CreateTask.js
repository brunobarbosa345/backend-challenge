module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Task", {
      id: {
        type: Sequelize.DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
      },
      notes: {
        type: Sequelize.DataTypes.STRING,
      },
      priority: {
        type: Sequelize.DataTypes.INTEGER,
        defaultValue: 0,
      },
      remindMeOn: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      activityType: {
        type: Sequelize.DataTypes.ENUM,
        allowNull: false,
        values: ["indoors", "outdoors"],
      },

      status: {
        type: Sequelize.DataTypes.ENUM,
        values: ["open", "done"],
        defaultValue: "open",
      },
      tags: {
        type: Sequelize.DataTypes.STRING,
      },

      taskListId: {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "TaskList",
          referencesKey: "id",
          onDelete: "CASCADE",
        },
      },

      createdAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      updatedAt: {
        defaultValue: null,
        type: Sequelize.DataTypes.DATE,
      },
      deletedAt: {
        type: Sequelize.DataTypes.DATE,
        defaultValue: null,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Task")
  },
}
