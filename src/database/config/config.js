const {
  DATABASE_USERNAME = "root",
  DATABASE_PASSWORD = "",
  DATABASE_HOST = "localhost",
  DATABASE_NAME = "task",
  DATABASE_DIALECT = "mysql",
  DATABASE_OPERATORS_ALIASE = "false",
} = process.env

module.exports = {
  username: DATABASE_USERNAME,
  password: DATABASE_PASSWORD,
  database: DATABASE_NAME,
  host: DATABASE_HOST,
  dialect: DATABASE_DIALECT,
  operatorsAliases: DATABASE_OPERATORS_ALIASE,
  dialectOptions: {
    dateStrings: true,
    typeCast: true,
  },
  options: {
    timezone: "America/Sao_Paulo",
  },
}
