//load envs (service)
require("services/environment")

import express from "express"
import moment from "moment"

//middlewares
import methodOverride from "method-override"
import bodyParser from "body-parser"
import compression from "compression"
import responseTime from "response-time"

import routes from "server/routes"

//services
import response from "services/response"

const { SERVER_HOST, SERVER_PORT } = process.env

const timeStart = moment()
const app = express()

//set info config in express
app.set("trust proxy", true)

//middlewares
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(methodOverride())
app.use(compression())
app.use(responseTime())

// if (process.env.NODE_ENV != "production") app.use(responseTime())

//routes ping
app.get("/", (req, res, next) => {
  response.success({}, { message: `Running on ${SERVER_HOST}:${SERVER_PORT}` })
  return res.status(response.info().status).send(response.info())
})
//routes cloud
app.get("/health-check", (req, res, next) => {
  response.success({}, { message: `Uptime ${timeStart.fromNow()}` })
  return res.status(response.info().status).send(response.info())
})

//routes system
app.use("/v1/tag", routes.tagRoutes)
app.use("/v1/task", routes.taskRoutes)
app.use("/v1/task-list", routes.taskListRoutes)
app.use("/v1/task-tag", routes.taskTagRoutes)

//not found
app.use("*", (req, res, next) => {
  response.success({}, { message: `Page not found`, status: 404 })
  return res.status(response.info().status).send(response.info())
})

app.listen(SERVER_PORT, SERVER_HOST, () => {
  console.log(`Running ${SERVER_HOST}:${SERVER_PORT}`)
})
