import { Router } from "express"
import { taskController } from "controller"
import response from "services/response"

const router = Router()

router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskResponse = await taskController.find(id)
    return res.status(taskResponse.status).send(taskResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.get("/", async (req, res, next) => {
  try {
    const taskResponse = await taskController.findAll()
    return res.status(taskResponse.status).send(taskResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.post("/", async (req, res, next) => {
  try {
    const body = req.body
    const taskResponse = await taskController.create(body)
    return res.status(taskResponse.status).send(taskResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.put("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const body = req.body
    const taskResponse = await taskController.update(id, body)
    return res.status(taskResponse.status).send(taskResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskResponse = await taskController.delete(id)
    return res.status(taskResponse.status).send(taskResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

export default router
