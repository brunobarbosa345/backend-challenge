import { Router } from "express"
import { tagController } from "controller"
import response from "services/response"

const router = Router()

router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const tagResponse = await tagController.find(id)
    return res.status(tagResponse.status).send(tagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.get("/", async (req, res, next) => {
  try {
    const tagResponse = await tagController.findAll()
    return res.status(tagResponse.status).send(tagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.post("/", async (req, res, next) => {
  try {
    const body = req.body
    const tagResponse = await tagController.create(body)
    return res.status(tagResponse.status).send(tagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.put("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const body = req.body
    const tagResponse = await tagController.update(id, body)
    return res.status(tagResponse.status).send(tagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const tagResponse = await tagController.delete(id)
    return res.status(tagResponse.status).send(tagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

export default router
