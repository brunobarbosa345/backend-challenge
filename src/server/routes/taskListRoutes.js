import { Router } from "express"
import { taskListController } from "controller"
import response from "services/response"

const router = Router()

router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskListResponse = await taskListController.find(id)
    return res.status(taskListResponse.status).send(taskListResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.get("/", async (req, res, next) => {
  try {
    const taskListResponse = await taskListController.findAll()
    return res.status(taskListResponse.status).send(taskListResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.post("/", async (req, res, next) => {
  try {
    const body = req.body
    const taskListResponse = await taskListController.create(body)
    return res.status(taskListResponse.status).send(taskListResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.put("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const body = req.body
    const taskListResponse = await taskListController.update(id, body)
    return res.status(taskListResponse.status).send(taskListResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskListResponse = await taskListController.delete(id)
    return res.status(taskListResponse.status).send(taskListResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

export default router
