import tagRoutes from "./tagRoutes"
import taskRoutes from "./taskRoutes"
import taskListRoutes from "./taskListRoutes"
import taskTagRoutes from "./taskTagRoutes"

export default {
  tagRoutes,
  taskRoutes,
  taskListRoutes,
  taskTagRoutes,
}
