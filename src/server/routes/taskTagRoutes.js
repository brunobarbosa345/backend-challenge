import { Router } from "express"
import { taskTagController } from "controller"
import response from "services/response"

const router = Router()

router.get("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskTagResponse = await taskTagController.find(id)
    return res.status(taskTagResponse.status).send(taskTagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.get("/", async (req, res, next) => {
  try {
    const taskTagResponse = await taskTagController.findAll()
    return res.status(taskTagResponse.status).send(taskTagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.post("/", async (req, res, next) => {
  try {
    const body = req.body
    const taskTagResponse = await taskTagController.create(body)
    return res.status(taskTagResponse.status).send(taskTagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.put("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const body = req.body
    const taskTagResponse = await taskTagController.update(id, body)
    return res.status(taskTagResponse.status).send(taskTagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id
    const taskTagResponse = await taskTagController.delete(id)
    return res.status(taskTagResponse.status).send(taskTagResponse)
  } catch (err) {
    response.error(err)
    return res.status(response.status).send(response)
  }
})

export default router
